FROM python:3.10-alpine

WORKDIR /app

RUN apk update

RUN apk add git

COPY ./start_project.sh /app/start_project.sh

RUN adduser -D -g '' user

RUN chown -R user:user /app

ENV PATH="$PATH:/home/user/.local/bin"

USER user

RUN pip install --upgrade pip && pip install --user cookiecutter

ENTRYPOINT ["/bin/sh", "./start_project.sh"]
