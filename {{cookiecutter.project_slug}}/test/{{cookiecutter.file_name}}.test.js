import { {{cookiecutter.func_name}} } from '../src/{{cookiecutter.file_name}}';

test("{{cookiecutter.func_name}} adds 1 + 2 to equal 3", () => {
  expect({{cookiecutter.func_name}}(1, 2)).toBe(3);
});
